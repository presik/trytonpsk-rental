# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.report import Report


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    factor = fields.Float('Factor', help="In Days")
    stock_qty = fields.Numeric('Stock Quantity')
    move_qty = fields.Float('Move Qty')
    shipment_number = fields.Char('Shipment Number')
    shipment_date = fields.Date('Shipment Date')


class RentalSaleRelationReport(Report):
    'Rental Sale Relation Report'
    __name__ = 'rental.sale_relation.report'
