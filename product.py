# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta


class ProductCategory(metaclass=PoolMeta):
    __name__ = 'product.category'
    leasable = fields.Boolean('Leasable')


class ProductTemplate(metaclass=PoolMeta):
    __name__ = 'product.template'
    leasable = fields.Boolean('Leasable')


class ProductCheckList(ModelView, ModelSQL):
    "Product Check List"
    __name__ = 'rental.service.product_check_list'
    rental_service = fields.Many2One('rental.service', 'Rental Service',
        required=True)
    item = fields.Many2One('product.check_list_item', 'Item',
        required=True)
    kind = fields.Selection([
            ('item', 'Item'),
            ('category', 'Category'),
        ], 'Kind')
    state_delivery = fields.Boolean('State Delivery')
    state_return = fields.Boolean('State Return')


class CheckListItem(ModelSQL, ModelView):
    "Check List Item"
    __name__ = "product.check_list_item"
    name = fields.Char('Name Item', required=True)
    kind = fields.Selection([
            ('category', 'Category'),
            ('item', 'Item'),
        ], 'Kind')
    parent = fields.Many2One('product.check_list_item', 'Parent')
    childs = fields.One2Many('product.check_list_item', 'parent', string='Children')

    @staticmethod
    def default_kind():
        return 'item'
