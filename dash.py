# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.dash.dash import DashAppBase

# cada clase que registro la debo declarar en _init,py
class DashApp(metaclass=PoolMeta):
    __name__ = "dash.app"

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend(
            [
                "dash.app.rental",
                "dash.app.rentalb",
                "dash.app.web_booking_rental",
            ]
        )
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend(
            [
                ("rental", "Rental"),
                ("rentalb", "Rentalb"),
                ("web_booking_rental", "Web Booking Rental"),
            ]
        )
        return options


class AppRental(DashAppBase):
    "Sale rental"
    __name__ = "dash.app.rental"

class AppRentalb(DashAppBase):
    "Sale rentalb"
    __name__ = "dash.app.rentalb"

class AppWebBookingRental(DashAppBase):
    "App Web BookingRental"
    __name__ = "dash.app.web_booking_rental"





