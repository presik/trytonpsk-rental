# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSingleton, ModelSQL, ModelView, fields
from trytond.pyson import Id


class Configuration(ModelSingleton, ModelSQL, ModelView):
    "Rental Configuration"
    __name__ = 'rental.configuration'
    rental_uom = fields.Many2One('product.uom', 'UoM Rental')
    rental_contract = fields.Text('Rental Contract')
    default_service = fields.Many2One('product.product', 'Default Service',
        domain=[
            ('type', '=', 'service'),
        ], required=True)
    rental_service_sequence = fields.Many2One('ir.sequence',
        'Rental Service Sequence', domain=[
            ('sequence_type', '=',
                Id('rental', 'sequence_type_rental')),
        ], required=True)
    rental_booking_sequence = fields.Many2One('ir.sequence',
        'Rental Booking Sequence', domain=[
            ('sequence_type', '=',
                Id('rental', 'sequence_type_rental')),
        ], required=True)
    value_early_return = fields.Numeric('Value to Early Return', digits=(16, 2),
        required=True)
    booking_start_time = fields.Time('Booking Start Time')
    booking_end_time = fields.Time('Booking End Time')
    default_check_list = fields.Many2Many(
        'rental.configuration-product.check_list_item', 'configuration',
        'check_list_item', 'Default Check List')
    email_booking_customer = fields.Many2One('email.template',
        'Email Booking Customer')
    email_booking_company = fields.Many2One('email.template',
        'Email Booking Company')
    email_rental_customer = fields.Many2One('email.template',
        'Email Rental Customer')
    email_rental_company = fields.Many2One('email.template',
        'Email Rental Company')
    email_rental_return_customer = fields.Many2One('email.template',
        'Email Rental Return Customer')
    email_rental_return_company = fields.Many2One('email.template',
        'Email Rental Return Company')
    email_rental_expire_company = fields.Many2One('email.template',
        'Email Rental Expire Company')
    email_rental_expire_customer = fields.Many2One('email.template',
        'Email Rental Expire Customer')
    email_rental_return_deposit = fields.Many2One('email.template',
        'Email Rental Return Deposit')
    email_rental_signature = fields.Many2One('email.template',
        'Email Send Signature')
    uom_hiring_time = fields.Selection([
            ('hour', 'Hour'),
            ('day', 'Day'),
            ('week', 'Week'),
            ('month', 'Month'),
        ], 'UoM Hiring Time')
    # sms_customer_pickup = fields.Many2One('sms.template',
    #     'SMS Customer Pickup')
    # email_template_customer_return = fields.Many2One('email.template',
    #     'Template Customer Return Service')

    @staticmethod
    def default_uom_hiring_time():
        return 'week'


class RentalConfigCheckList(ModelSQL):
    "Rental Config - Check List"
    __name__ = 'rental.configuration-product.check_list_item'
    _table = 'rental_config_product_check_list_item_rel'
    configuration = fields.Many2One('rental.configuration', 'Configuration',
        ondelete='CASCADE', required=True)
    check_list_item = fields.Many2One('product.check_list_item', 'Check List Item',
        ondelete='RESTRICT', required=True)
