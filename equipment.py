# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, Unique
from trytond.pool import Pool, PoolMeta


class RentalEquipment(metaclass=PoolMeta):
    __name__ = 'maintenance.equipment'
    status = fields.Selection([
        ('available', 'Available'),
        ('maintenance', 'Maintenance'),
        ('not_available', 'Not Available'),
        ('stolen', 'Stolen'),
        ], 'Status')
    issues = fields.Text('issues')
    gps_passwd = fields.Char('GPS Password')
    gps_link = fields.Char('GPS Link')
    date_return = fields.Function(fields.Date('Date Return'), 'get_return_date')
    current_service = fields.Function(fields.Many2One(
        'rental.service', 'Contract Service'), 'get_service')

    @classmethod
    def __setup__(cls):
        super(RentalEquipment, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
           ('product_uniq', Unique(table, table.product),
               'Product already exists!'),
        ]

    @staticmethod
    def default_status():
        return 'available'

    def get_return_date(self, name):
        if self.current_service:
            return self.current_service.end_date

    def get_service(self, name):
        Service = Pool().get('rental.service')
        services = Service.search([
            ('state', 'in', ['confirmed', 'pickup', 'renovated']),
            ('equipment', '=', self.id)
        ], order=[('end_date', 'DESC')])
        if not services:
            return None
        service = services[0]
        return service.id

    def get_rec_name(self, name):
        product_name = self.product.name or ''
        if self.vin:
            product_name = '[' + self.vin + ']' + product_name
        return product_name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [
            bool_op,
            ('product.code',) + tuple(clause[1:]),
            ('product.name',) + tuple(clause[1:]),
            ('vin',) + tuple(clause[1:]),
        ]
