# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import service
from . import booking
from . import configuration
from . import product
from . import sale
from . import invoice
from . import stock
from . import equipment
from . import dash



def register():
    Pool.register(
        configuration.Configuration,
        service.DepositLine,
        service.Service,
        service.RentalVoucher,
        service.RentalSale,
        booking.Booking,
        product.ProductCheckList,
        product.CheckListItem,
        product.ProductCategory,
        product.ProductTemplate,
        configuration.RentalConfigCheckList,
        sale.SaleLine,
        invoice.InvoiceLine,
        stock.ShipmentAdditional,
        stock.CreateSalesFromMovesStart,
        stock.Location,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        stock.Move,
        equipment.RentalEquipment,
        dash.DashApp,
        dash.AppRental,
        dash.AppRentalb,
        dash.AppWebBookingRental,

        module='rental', type_='model')
    Pool.register(
        service.ServiceForceDraft,
        stock.CreateSalesFromMoves,
        stock.ShipmentOutForceDraft,
        stock.ShipmentOutReturnForceDraft,
        service.CacheExpirationDays,
        module='rental', type_='wizard')
    Pool.register(
        service.RentalServiceReport,
        service.RentalEquipmentReturnReport,
        sale.RentalSaleRelationReport,
        module='rental', type_='report')
